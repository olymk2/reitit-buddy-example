(ns api.permissions
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.core.nonce :as nonce]
            [buddy.sign.jwt :as jwt]
            [nano-id.core :refer [nano-id]]
            [ring.middleware.session.memory :as memory]
            [ring.util.http-response :as resp]))

(def jwt-secret "shared-secret-key-here")

(def data (nonce/random-bytes 1024))

;; when authing using sessions store the users session in memory here
(def session-store-atom (atom {}))
(def session-store (memory/memory-store session-store-atom))

;demo tokens, use something like nano-id to generate a token and store in a db
(def tokens {:2f904e245c1f5 :admin :45c1f5e3f05d0 :user})

;demo login users would be db backed normally
(def users {:user :password123 :admin :password123})

;; Buddy works by checking for the :identity token, so if we populate the request
;; with the :identity key with a truth value ie true or a hash map for example
;; then authenticate? will pass

(defn remove-bearer
  "Little helper to remove the word Bearer"
  [header]
  (clojure.string/replace header #"Bearer " ""))

(defn authorization-login
  "Grabbing the user and password from the query params checking they exist
  Set identity so authorized? test passes should also check password here."
  [handler]
  (fn [{{{:keys [username password]} :query} :parameters :as request}]
    (if (contains? users (keyword username))
      (handler (-> request
                   (assoc :identity {:test "123"})
                   (assoc :session {:user "jim" :id (nano-id)})))
      (handler request))))

(defn auth
  "Middleware used in routes that require authentication. If request is not
   authenticated a 401 not authorized response will be returned checking for presence of :identity"
  [handler]
  (fn [request]
    (if (authenticated? request)
      (handler request)
      (resp/unauthorized {:error "Not authorized"}))))


;; force endpoint to require a matching token in the authorization header
(defn auth-jwt-token-required
  "This function checks if the authorization is set in the request header and that its in then valid list."
  [handler]
  (fn [request]
    (try
      (jwt/unsign (remove-bearer (-> request :parameters :header :authorization)) jwt-secret)
      ;; do db lookup from key in your token and return the map as identity value if preferred
      (handler (assoc request :identity true))
      (catch Exception e
        {:status 500
         :body {:msg (.getMessage e)
         :raw (str e)
         :data (ex-data e)}}))))

;; force endpoint to require a matching token in the authorization header
(defn auth-token-required
  "This function checks if the authorization is set in the request header and that its in then valid list."
  [handler]
  (fn [request]
    (if (some #{(-> request :parameters :header :authorization)} (map name (keys tokens)))
      (handler (assoc request :identity true))
      (handler request))))

(comment
  (jwt/sign {:user_id 1 :other "data"} "shared-secret-key-here"))
