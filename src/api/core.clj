(ns api.core
  (:gen-class)
  (:require [api.permissions :as perm :refer [auth]]
            [api.views :as view]
            [muuntaja.core :as m]
            reitit.coercion.spec
            [reitit.dev.pretty :as pretty]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as coercion]
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.session :as session]))

(def api-routes
  [["/swagger.json"
    {:get {:no-doc true
           :swagger {:info {:title "Reitit buddy auth example"
                            :description "Example integration"}}
           :handler (swagger/create-swagger-handler)}}]
   ["/api"
    {:summary "auth end points"
     :middleware [session/wrap-session {:store perm/session-store :cookie-attrs {:max-age 3600}}]}
    ["/status" {:get (fn [_] {:status 200 :body "ok"})}]
    ["/login"
     {:get {:middleware [perm/authorization-login auth]
            :summary "User login"
            :description "Login with user or admin and password123"
            :parameters {:query {:username string? :password string?}}
            :handler (fn [query]
                       {:status 200 :body (view/login query)})}}]

    ["/test/jwt" {:get {:parameters {:header {:authorization string?}}
                        :description "Given a jwt try and decode or show the error"
                        :summary "test jwt token unsign response"
                        :handler (fn [query]
                                   {:status 200 :body (view/test-jwt-unsign query)})}}]
    ["/restricted" {:summary "test various endpoint auths"}

     ["/token" {:get {:middleware [perm/auth-token-required auth]
                      :summary "Test auth via token"
                      :description "Login grab the token and use it here"
                      :parameters {:header {:authorization string?}}
                      :handler (fn [_] {:status 200 :body "valid token"})}}]

     ["/jwt" {:get {:middleware [perm/auth-jwt-token-required auth]
                    :summary "Test auth via jwt token"
                    :descriptiobn "Login and grab the jwt token and paste it here"
                    :parameters {:header {:authorization string?}}
                    :handler (fn [_] {:status 200 :body "valid jwt"})}}]

     ["/session"
      {:get {:middleware [auth]
             :description "Not implemented yet"
             :summary "Test auth via session"
             :handler (fn [_] {:status 200 :body "ok"})}}]]]])

(def api-routes-config
  {:exceptions pretty/exception
   :data {:coercion reitit.coercion.spec/coercion
          :muuntaja m/instance
          :middleware [;; swagger feature
                       swagger/swagger-feature
                        ;; query-params & form-params
                       parameters/parameters-middleware
                        ;; content-negotiation
                       muuntaja/format-negotiate-middleware
                        ;; encoding response body
                       muuntaja/format-response-middleware
                        ;; exception handling
                       exception/exception-middleware
                        ;; decoding request body
                       muuntaja/format-request-middleware
                        ;; coercing response bodys
                       coercion/coerce-response-middleware
                        ;; coercing request parameters
                       coercion/coerce-request-middleware
                        ;; multipart
                        ;;multipart/multipart-middleware
                       ]}})
(def app-reitit
  (ring/ring-handler
   (ring/router
    api-routes
    api-routes-config)
    ;; optional default ring handler (if no routes have matched)
   (ring/routes
    (swagger-ui/create-swagger-ui-handler
     {:path "/"
      :config {:validatorUrl nil
               :operationsSorter "alpha"}})
    (ring/create-default-handler)
    ;; the middleware on ring-handler runs before routing
    {:middleware [session/wrap-session]})))

(defn launch-api [& args]
  (prn "launching on 4000")
  (run-jetty (wrap-reload #'app-reitit) {:port 4000}))

(defn -main [& args]
  (launch-api args))
