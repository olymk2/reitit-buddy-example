(ns api.views
  (:gen-class)
  (:require [api.permissions :as perm]
            [buddy.sign.jwt :as jwt]))

(defn login
  "Return a token jwt token and session to test with"
  [query]
  {:session  (:session query)
   :params (-> query :parameters :query :username)
   :jwt-token (jwt/sign {:user (-> query :parameters :query :username)} perm/jwt-secret)
   :token (str
           (->> perm/tokens
                (filter (fn [[_ v]] (= (keyword "admin") v)))
                (first)
                (first)))})

(defn generate-jwt []
  (jwt/sign {:user_id 1 :other "data"} "shared-secret-key-here"))

(defn test-jwt-unsign
  "Try to unsign the token in an unrestricted view and show error message"
  [query]
  (try
    (jwt/unsign
     (perm/remove-bearer
      (-> query :parameters :header :authorization))
     perm/jwt-secret)
    (catch Exception e
      {:msg (.getMessage e)
       :data (ex-data e)})))
